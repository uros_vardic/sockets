package croupier;

import message.MessageManager;
import message.MessageType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.BrokenBarrierException;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class PlayerHandler implements Runnable {

    private final Croupier croupier;

    private final Socket player;

    private final BufferedReader input;

    private final PrintWriter output;

    private final int playerIndex;

    PlayerHandler(Croupier croupier, Socket player, int playerIndex, BufferedReader input, PrintWriter output) {
        checkArgument(playerIndex >= 0);

        this.croupier    = checkNotNull(croupier);
        this.player      = checkNotNull(player);
        this.playerIndex = playerIndex;
        this.input       = checkNotNull(input);
        this.output      = checkNotNull(output);
    }

    public int getPlayerIndex() {
        return playerIndex;
    }

    private int currentRound;

    @Override
    public void run() {
        croupier.addScore(this, 0);
        currentRound = croupier.isRoundSet() ? croupier.getCurrentRound() + 1 : croupier.getCurrentRound();

        try {
            MessageManager.sendCroupierMessage("Connection successful! Waiting for other players...",
                    output, playerIndex);

            while (!gameEnded()) {
                croupier.getRoundBarrier().await();

                printRoundInfo();
                sendRoundInfo();

                if (isPlayerDrawing())
                    draw();
                else
                    predict();

                if (playerLost) {
                    MessageManager.printSystemMessage("Player " + playerIndex + " LOST!");
                    closePlayerHandler();
                    break;
                }

                croupier.setCurrentRound(currentRound++);
            }

            closePlayerHandler();

            if (gameEnded()) {
                Thread.sleep(500L);
                MessageManager.printScore(croupier.getScores());

                System.exit(0);
            }
        } catch (InterruptedException | BrokenBarrierException | IOException e) {
            e.printStackTrace();
        }

    }

    private boolean gameEnded() {
        return croupier.getNumberOfSticks() == 0 || lastRound();
    }

    private boolean lastRound() {
        return croupier.getCurrentRound() == croupier.getNumberOfRounds();
    }

    private void printRoundInfo() {
        MessageManager.printRoundInfo(currentRound);
        MessageManager.restartRoundPrint();
    }

    private void sendRoundInfo() {
        MessageManager.sendMessage(currentRound + "", output);
        MessageManager.sendMessage(croupier.getNumberOfSticks() + "", output);
    }

    private boolean isPlayerDrawing() {
        final int drawIndex = currentRound % croupier.getNumberOfPlayers();

        if (drawIndex == 0)
            if (playerIndex == croupier.getNumberOfPlayers())
                return true;

        return drawIndex == playerIndex;
    }

    private static boolean isStickShort;

    private boolean playerLost;

    private void draw() throws BrokenBarrierException, InterruptedException, IOException {
        MessageManager.sendCroupierMessage("Your turn to draw!", MessageType.DRAW, output, playerIndex);

        final int stickIndex = Integer.parseInt(MessageManager.paresMessageFromInput(input).getMessage());

        playerLost = isStickShort = croupier.drawStick(stickIndex);

        croupier.getRoundBarrier().await();

        MessageManager.printPlayerMessage("Drew stick " + stickIndex + ", is short: " + isStickShort,
                playerIndex);

        MessageManager.sendMessage("Is stick short " + isStickShort, output);
    }

    private void predict() throws BrokenBarrierException, InterruptedException, IOException {
        MessageManager.sendCroupierMessage("Your turn to predict!", MessageType.PREDICT, output, playerIndex);

        final boolean playerPrediction = Boolean.parseBoolean(MessageManager.paresMessageFromInput(input).getMessage());

        MessageManager.printPlayerMessage("Predicts " + playerPrediction, playerIndex);

        croupier.getRoundBarrier().await();

        if (isStickShort == playerPrediction) {
            croupier.addScore(this, croupier.getScore(this) + 1);
            MessageManager.sendCroupierMessage("Prediction true! Awarded 1 point", MessageType.SCORE, output,
                    playerIndex);
            MessageManager.printCroupierMessage("Player " + playerIndex + " scored 1 point!");
        } else
            MessageManager.sendCroupierMessage("Prediction false!", output, playerIndex);
    }

    private void closePlayerHandler() throws IOException {
        MessageManager.sendMessage("Game over", MessageType.EXIT, output);
        croupier.removePlayer(this);
        croupier.prepareSticks();
        output.close();
        input.close();
        player.close();
    }

}
