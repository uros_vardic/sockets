package util;

public class AppConstants {

    public static final int NUMBER_OF_PLAYERS = 6;

    public static final int NUMBER_OF_ROUNDS = 8;

    private AppConstants() {}

}
