package message;

import com.google.gson.Gson;
import croupier.PlayerHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import static util.Preconditions.checkNotNull;

// Bunch of methods for sending JSON messages back and forth between players and croupier
public class MessageManager {

    private static final Gson JSONParser = new Gson();

    private final String message;

    private final MessageType type;

    private MessageManager(String message, MessageType type) {
        this.message = message;
        this.type    = type;
    }

    public String getMessage() {
        return message;
    }

    public MessageType getType() {
        return type;
    }

    private static String toJSON(String message) {
        return JSONParser.toJson(new MessageManager(checkNotNull(message), MessageType.PLAIN));
    }

    private static String toJSON(String message, MessageType type) {
        return JSONParser.toJson(new MessageManager(checkNotNull(message), checkNotNull(type)));
    }

    private static MessageManager fromJSON(String JSON) {
        return JSONParser.fromJson(checkNotNull(JSON), MessageManager.class);
    }

    public static MessageManager paresMessageFromInput(BufferedReader input) throws IOException {
        final String messageJSON = checkNotNull(input).readLine();

        return MessageManager.fromJSON(messageJSON);
    }

    public static void sendMessage(String message, PrintWriter output) {
        checkNotNull(output).println(toJSON(checkNotNull(message)));
    }

    public static void sendMessage(String message, MessageType type, PrintWriter output) {
        checkNotNull(output).println(toJSON(checkNotNull(message), checkNotNull(type)));
    }

    public static void sendCroupierMessage(String message, PrintWriter output, int playerIndex) {
        final String finalMessage = "[CROUPIER -> PLAYER " + playerIndex + "]: " + checkNotNull(message);

        checkNotNull(output).println(toJSON(finalMessage));
    }

    public static void sendCroupierMessage(String message, MessageType type, PrintWriter output, int playerIndex) {
        final String finalMessage = "[CROUPIER -> PLAYER " + playerIndex + "]: " + checkNotNull(message);

        checkNotNull(output).println(toJSON(finalMessage, checkNotNull(type)));
    }

    public static void printMessage(MessageManager messageManager) {
        if (isErrorMessage(messageManager))
            System.err.println(messageManager.getMessage());
        else
            System.out.println(messageManager.getMessage());
    }

    public static boolean isErrorMessage(MessageManager messageManager) {
        return messageManager.type.equals(MessageType.ERROR) || messageManager.type.equals(MessageType.FULL_TABLE);
    }

    public static void printSystemMessage(String message) {
        System.out.println("[SYSTEM]: " + message);
    }

    public static void printCroupierMessage(String message) {
        System.out.println("[CROUPIER]: " + message);
    }

    public static void printPlayerMessage(String message, int playerIndex) {
        System.out.println("[PLAYER " + playerIndex + "]: " + message);
    }

    public static void restartConsole() {
        System.out.println("\033[H\033[2J");
    }

    private static boolean singlePrint;

    public synchronized static void singlePrint(String message) {
        if (singlePrint)
            return;

        singlePrint = true;

        System.out.println("[CROUPIER -> ALL]: " + message);
    }

    public static void restartSingePrint() {
        // short delay for better printing and readability
        try {
            Thread.sleep(500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        singlePrint = false;
    }

    private static boolean roundPrint;

    public synchronized static void printRoundInfo(int roundNumber) {
        if (roundPrint)
            return;

        roundPrint = true;

        final String message = "\n==========================================================================\n" +
                               "**********************************ROUND " + roundNumber + "*********************************\n" +
                               "==========================================================================\n";

        System.out.println(message);
    }

    public static void restartRoundPrint() {
        // short delay for better printing and readability
        try {
            Thread.sleep(500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        roundPrint = false;
    }

    private static boolean scorePrint;

    public synchronized static void printScore(Map<PlayerHandler, Integer> scores) {
        if (scorePrint)
            return;

        scorePrint = true;

        final String message = "\n==========================================================================\n" +
                               "************************************SCORE*********************************\n" +
                               "==========================================================================\n";

        System.out.println(message);

        final PlayerHandler winner = Collections.max(scores.entrySet(), Comparator.comparingInt(Map.Entry::getValue))
                .getKey();

        System.out.println("[WINNER]: Player " + winner.getPlayerIndex() + " score = " + scores.get(winner));
    }

}
