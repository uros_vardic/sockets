package player;

import java.io.IOException;
import java.util.stream.IntStream;

import static util.AppConstants.NUMBER_OF_PLAYERS;

class PlayerApp {

    public static void main(String[] args) {
        IntStream.range(0, NUMBER_OF_PLAYERS)
                .forEach(counter -> new Thread(createPlayer()).start());
    }

    private static Player createPlayer() {
        try {
            return new Player();
        } catch (IOException e) {
            e.printStackTrace();
        }

        throw new RuntimeException();
    }

}
