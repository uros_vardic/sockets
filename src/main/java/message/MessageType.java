package message;

public enum MessageType {

    PLAIN, ERROR, PREDICT, DRAW, SCORE, FULL_TABLE, EXIT

}
