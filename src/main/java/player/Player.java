package player;

import croupier.Croupier;
import message.MessageManager;
import message.MessageType;

import java.io.*;
import java.net.Socket;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

class Player implements Runnable {

    private static final String HOST_NAME = "localhost";

    private static final long MIN_DELAY_TIME = 0L;

    private static final long MAX_DELAY_TIME = 1001L;

    private final UUID uuid = UUID.randomUUID();

    private final Socket socket = new Socket(HOST_NAME, Croupier.PORT);

    private final BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

    private final PrintWriter output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

    Player() throws IOException {}

    private long calculateDelayTime() {
        return ThreadLocalRandom.current().nextLong(MIN_DELAY_TIME, MAX_DELAY_TIME);
    }

    private boolean gameEnded;

    private int score;

    @Override
    public void run() {
        // Tell the croupier how long will it take for the player to get ready
        MessageManager.sendMessage(calculateDelayTime() + "", output);

        try {
            if (!applyForTable()) return;

            while (!gameEnded) {
                roundInfo();
                playRound();
            }

            closePlayer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean applyForTable() throws IOException {
        final MessageManager welcomeMessage = MessageManager.paresMessageFromInput(input);

        if (MessageManager.isErrorMessage(welcomeMessage)) {
            MessageManager.printMessage(welcomeMessage);
            return false;
        }

        MessageManager.printMessage(welcomeMessage);
        return true;
    }

    private int numberOfSticks;

    private void roundInfo() throws IOException {
        final MessageManager roundInfo = MessageManager.paresMessageFromInput(input);

        if (roundInfo.getType().equals(MessageType.EXIT)) {
            gameEnded = true;
            return;
        }

        final int round = Integer.parseInt(roundInfo.getMessage());

        MessageManager.printRoundInfo(round);

        numberOfSticks = Integer.parseInt(MessageManager.paresMessageFromInput(input).getMessage());

        MessageManager.restartRoundPrint();
    }

    private void playRound() throws IOException {
        if (gameEnded)
            return;

        final MessageManager roundMessage = MessageManager.paresMessageFromInput(input);

        MessageManager.printMessage(roundMessage);

        if (roundMessage.getType().equals(MessageType.DRAW)) {
            MessageManager.sendMessage(ThreadLocalRandom.current().nextInt(0, numberOfSticks) + "", output);
            MessageManager.singlePrint(MessageManager.paresMessageFromInput(input).getMessage());
            MessageManager.restartSingePrint();
        }

        else if (roundMessage.getType().equals(MessageType.PREDICT)) {
            MessageManager.sendMessage(ThreadLocalRandom.current().nextBoolean() + "", output);

            final MessageManager scoreMessage = MessageManager.paresMessageFromInput(input);

            if (scoreMessage.getType().equals(MessageType.SCORE))
                score++;

            MessageManager.printMessage(scoreMessage);
        }
    }

    private void closePlayer() throws IOException {
        input.close();
        output.close();
        socket.close();
    }

    @Override
    public String toString() {
        return String.format("Player[uuid=%s, socket=%s]", uuid, socket);
    }

    @Override
    public boolean equals(Object reference) {
        if (reference instanceof Player) {
            final Player other = (Player) reference;

            return Objects.equals(this.uuid, other.uuid);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(uuid);
    }
}
