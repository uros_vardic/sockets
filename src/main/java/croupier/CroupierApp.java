package croupier;

import java.io.IOException;

import static util.AppConstants.NUMBER_OF_PLAYERS;
import static util.AppConstants.NUMBER_OF_ROUNDS;

public class CroupierApp {

    public static void main(String[] args) {
        try {
            new Thread(new Croupier(NUMBER_OF_PLAYERS, NUMBER_OF_ROUNDS)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
