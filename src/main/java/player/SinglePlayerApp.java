package player;

import java.io.IOException;

class SinglePlayerApp {

    // Intended for adding a player to an existing game,
    // for a new game refer to PlayerApp class.
    public static void main(String[] args) {
        try {
            new Thread(new Player()).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
