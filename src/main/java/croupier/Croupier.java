package croupier;

import message.MessageManager;
import message.MessageType;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.IntStream;

import static util.Preconditions.checkArgument;
import static util.Preconditions.checkNotNull;

public class Croupier implements Runnable {

    public static final int PORT = 3000;

    private static final int SOCKET_TIMEOUT = 200;

    private final ServerSocket serverSocket = new ServerSocket(PORT);

    private final int numberOfPlayers;

    private final int numberOfRounds;

    private final ScheduledExecutorService playerExecutor;

    private final CyclicBarrier roundBarrier;

    private final List<Boolean> sticks = new CopyOnWriteArrayList<>();

    private final List<PlayerHandler> players = new CopyOnWriteArrayList<>();

    private final Map<PlayerHandler, Integer> scores = new HashMap<>();

    Croupier(int numberOfPlayers, int numberOfRounds) throws IOException {
        checkArgument(numberOfPlayers > 0, "Number of players must be positive number");
        checkArgument(numberOfRounds > 0, "Number of rounds must be positive number");

        this.numberOfPlayers = numberOfPlayers;
        this.numberOfRounds  = numberOfRounds;
        this.playerExecutor  = Executors.newScheduledThreadPool(numberOfPlayers);
        this.roundBarrier    = new CyclicBarrier(numberOfPlayers);

        serverSocket.setSoTimeout(SOCKET_TIMEOUT);
    }

    int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    void removePlayer(PlayerHandler player) {
        players.remove(checkNotNull(player));
        gameInProgress = false;
    }

    int getNumberOfRounds() {
        return numberOfRounds;
    }

    synchronized int getNumberOfSticks() {
        return sticks.size();
    }

    boolean drawStick(int stickIndex) {
        final boolean stick = sticks.get(stickIndex);

        sticks.remove(stickIndex);

        return stick;
    }

    CyclicBarrier getRoundBarrier() {
        return roundBarrier;
    }

    synchronized void addScore(PlayerHandler player, int score) {
        scores.put(checkNotNull(player), score);
    }

    synchronized int getScore(PlayerHandler player) {
        return scores.get(player);
    }

    synchronized Map<PlayerHandler, Integer> getScores() {
        return Collections.unmodifiableMap(scores);
    }

    private int currentRound = 1;

    private boolean roundSet;

    int getCurrentRound() {
        return currentRound;
    }

    void setCurrentRound(int currentRound) {
        checkArgument(currentRound > 0 && currentRound <= numberOfRounds);

        this.currentRound = currentRound;
        roundSet = true;
    }

    boolean isRoundSet() {
        return roundSet;
    }

    private boolean gameInProgress;

    @Override
    public void run() {
        while (numberOfRounds != currentRound) {
            setupTable();
            prepareSticks();

            while (!tableIsFull()) {
                try {
                    connectPlayer();
                } catch (IOException ignored) {}
            }

            gameInProgress = true;
            MessageManager.printSystemMessage("Table is full!");

            while (gameInProgress) {
                try {
                    rejectPlayer();
                } catch (IOException ignored) {}
            }
        }
    }

    private void setupTable() {
        MessageManager.restartConsole();
        MessageManager.printSystemMessage("Waiting for players...");
        gameInProgress = false;
    }

    synchronized void prepareSticks() {
        sticks.clear();

        IntStream.range(0, numberOfPlayers)
                .forEach(index -> sticks.add(false));

        final int randomIndex = ThreadLocalRandom.current().nextInt(0, numberOfPlayers);

        sticks.set(randomIndex, true);
    }

    private boolean tableIsFull() {
        return players.size() == numberOfPlayers;
    }

    private void connectPlayer() throws IOException {
        final Socket player = serverSocket.accept();

        final BufferedReader input = new BufferedReader(new InputStreamReader(player.getInputStream()));

        final PrintWriter output = new PrintWriter(new OutputStreamWriter(player.getOutputStream()), true);

        final int playerIndex = players.size() + 1;

        final PlayerHandler playerHandler = new PlayerHandler(this, player, playerIndex, input, output);

        players.add(playerHandler);

        MessageManager.printSystemMessage("Player " + players.size() + " joined the table!");

        final long delayTime = Long.parseLong(MessageManager.paresMessageFromInput(input).getMessage());

        playerExecutor.schedule(playerHandler, delayTime, TimeUnit.MILLISECONDS);
    }

    private void rejectPlayer() throws IOException {
        final Socket player = serverSocket.accept();

        // Croupier dose not appoint a player handle when he is about to reject a player
        // Player is closed right here
        final PrintWriter output = new PrintWriter(new OutputStreamWriter(player.getOutputStream()), true);

        MessageManager.sendMessage("[CROUPIER]: Table is full! Comeback later!", MessageType.FULL_TABLE, output);

        player.close();
    }

}
